<?php

require 'vendor/autoload.php';

use \app\controler\ControlerCatalogue;
use \app\controler\ControlerUser;
use \app\controler\ControlerConnexion;
use \app\controler\ControlerInscription;
use \app\controler\ControlerGroupe;


use \app\model\DBConnection;

DBConnection::getInstance();
if(!isset($_SESSION)){
	session_start();
}

$app = new \Slim\Slim();

$app->get('/inscription', function(){
	(new ControlerInscription())->pageInscription();
})->name('inscription');

$app->get('/connexion', function(){
	(new ControlerConnexion())->pageConnexion();
})->name('connexion');

$app->get('/users', function(){
	(new ControlerUser())->afficherListeUtilisateurs();
})->name('users');

$app->get('/user/:id', function($id){
	(new ControlerUser())->afficherUtilisateur($id);
})->name('user');

$app->get('/css',function(){})->name('css');

$app->get('/fonts',function(){})->name('fonts');

$app->get('/img',function(){})->name('img');

$app->get('/js',function(){})->name('js');

$app->get('/', function(){
	(new ControlerCatalogue())->index();
})->name('racine');

$app->get('/continuer', function(){
	(new ControlerCatalogue())->continuer();
})->name('continuer');

$app->get('/catalogue/:id', function($id){
	(new ControlerCatalogue())->afficherLogement($id);
})->name('logement');

$app->get('/inscriptionLogement/:id', function($id){
	(new ControlerGroupe())->inscriptionLogement($id);
})->name('InscriptionLogement');

$app->post('/groupe/', function(){
	(new ControlerGroupe())->Groupe();
})->name('groupe');


$app->post('/sendconnexion', function(){
	(new ControlerConnexion())->sendConnexion();
})->name('sendConnexion');

$app->get('/logout', function(){
	(new ControlerConnexion())->logout();
})->name('logout');

$app->run();
