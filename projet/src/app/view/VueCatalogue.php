<?php

namespace app\view;
use \app\controler\Controler;


class VueCatalogue{
	const INDEX=0;
	const LIST_LOGEMENT = 1;
	const ONE_LOGEMENT = 2;

	private $liste_logem;
	function __construct($list=null)
	{
		$this->liste_logem=$list;
	}

	private function connexion(){
		$app=\Slim\Slim::getInstance();
		$res='<div style="height:80%;width:90%" class="container ">
        <div class="row " style="height:100%;width:100%;margin-top:3.5%;">
      <div style="margin-top:8.5%" class="col s6 center-align "><img style="height:75%;width:100%;border-radius:25px;" class="responsive-img " src="img/house.jpg"></div>
      <div style="height:100%;" class="col s6  "><p style="margin-top:20%"> <blockquote>
Parentes amicitia cum si quae quidem moribus cum homine sit illa mihi probitatis appareat Quod parentes quale amantur ex primum detestabili in ab amoris in eis ita homine exstitit applicatione natos ab deinde quae natos cuius deinde amant perspicere potest similis potest quae multo quasi scelere cum ita quodam detestabili scelere natos utilitatis ea bestiis perspicere a appareat cum in et amoris earum ita quam cum cum multo et quale non sit virtutis perspicere quod orta potius deinde sumus ab quibusdam utilitatis cum detestabili cum ab potius non tempus probitatis detestabili in videtur potius sumus nisi Quod quae nacti dirimi.</br>Parentes amicitia cum si quae quidem moribus cum homine sit illa mihi probitatis appareat Quod parentes quale amantur ex primum detestabili in ab amoris in eis ita homine exstitit applicatione natos ab deinde quae natos cuius deinde amant perspicere potest similis potest quae multo quasi scelere cum ita quodam detestabili scelere natos utilitatis ea bestiis perspicere a appareat cum in et amoris earum ita quam cum cum multo et quale non sit virtutis perspicere quod orta potius deinde sumus ab quibusdam utilitatis cum detestabili cum ab potius non tempus probitatis detestabili in videtur potius sumus nisi Quod quae nacti dirimi. </blockquote>
</p><a href="'.$app->urlFor('continuer').'" style="width:100%; border-radius:12px;" class="waves-effect waves-light btn-large">Continuer</a></div>
    </div>
   </div>';

		return $res;

	}

	private function afficherListeLogement($liste){
		$app=\Slim\Slim::getInstance();
		$res = '<div class="row">
							<div class="col s12">
								<h4>Logements :</h4>
							</div>
						</div>';
		foreach($liste as $key=>$val){
      $res.='<div class="row">
        <div class="col s12 m4 l2">
        </div>
        <div class="col s12 m4 l8">
          <div class="card-panel grey lighten-5 z-depth-1">

              <div class="row valign-wrapper" style="cursor:pointer" onclick="document.location=\''.$app->urlFor('logement',array('id'=>$val['id'])).'\';">
                <div class="col s2">
                  <img src="' . $app->urlFor('img') . '/apart/' . $val["id"] . '.jpg" alt="" class="responsive-img"> <!-- notice the "circle" class -->
                </div>
                <div class="col s10">
                  <span class="black-text">
                      <h5>'.$val["libelle"].'</h5>
                      <p>'.$val['places'].' places</p>
                  </span>
                </div>
              </div>
          </div>
        </div>
        <div class="col s12 m4 l2">
        </div>
      </div>';
		}
		return $res;
	}

	public function afficherLogement($val){
		$app=\Slim\Slim::getInstance();
		$res = "";
		$res.='<div class="col s12 m4 l8">
			<div class="card-panel grey lighten-5 z-depth-1">
				<div class="row valign-wrapper" >
						<div class="col s2">
						</div>
						<div class="col s2">
							<img src="' . $app->urlFor('img') . '/apart/' . $val["id"] . '.jpg"class="responsive-img" alt="" >
						</div>
						<div class="col s6">
							<span class="black-text">
                     			 <h5>'.$val["libelle"].'</h5>
                    			  <p>'.$val['places'].' places</p>';
                    			  		if(isset($_SESSION['done'])){
												$res.='<a class ="waves-effect waves-light btn-large" href="'.$app->urlFor('InscriptionLogement', array('id' => $val["id"])).'">Inscription</a>';
                    			  		}		
		$res.='</span>
						</div>
                    	<div class="col s2">
						</div>
					</div>
                </div>
			</div>
		</div>';
	return $res;

	}

	public function render($selecteur)
	{
		$res;
		switch($selecteur)
		{
			case self::INDEX:
			$res=$this->connexion();
			break;
			case self::LIST_LOGEMENT:
			$res=$this->afficherListeLogement($this->liste_logem);
			break;
			case self::ONE_LOGEMENT:
			$res=$this->afficherLogement($this->liste_logem);
			break;
		}

		$app=\Slim\Slim::getInstance();
		$routecss=$app->urlFor('css');
		$routejs=$app->urlFor('js');
		$routefonts=$app->urlFor('fonts');
		$routeimg=$app->urlFor('img');
		$h=new VueHeader();
		if($selecteur==self::INDEX){
			$header=$h->headerFirst();
		}else{
			$header=$h->headerToHtml();
		}

		$html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>lebonlogement</title>
		<link rel="icon" type="image/png" href="$routeimg/icon_house.png" />
		<!--Import Google Icon Font-->
		 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		 <!--Import materialize.css-->
		 <link type="text/css" rel="stylesheet" href="$routecss/materialize.min.css"  media="screen,projection"/>

		 <!--Let browser know website is optimized for mobile-->
		 <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		 <script type="text/javascript" src="$routejs/materialize.min.js"></script>
	</head>
	<body style="background-color:#F5F5DC">
	$header
	<div class="content">
	$res
	</div>
	</body>
</html>
EOT;

		echo $html;
	}
}
