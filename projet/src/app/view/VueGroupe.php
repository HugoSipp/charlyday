<?php

namespace app\view;

class VueGroupe {
	
	const INSCRIPTION = 0;
	
	private $logem;
	function __construct($log)
	{
		$this->logem=$log;
	}
	
	
	public function inscriptionLogement($nbplaces){
		$app=\Slim\Slim::getInstance();
		$res = '<br>
				<h2 class="titre">Inscription dans logement</h2>
			<form class="form-style-9" action="'.$app->urlFor('groupe').'" method="post">
			<ul>';
			for($i=0;$i<$nbplaces;$i += 1){
				$res .='<li>
				<input type="email" name="mail'. $i .'"  class="field-style field-split " placeholder="E-mail" required ="required"/>
				</li>';
			}
			$res.='</li>
			<li>
			<input type="submit" value="Inscrire Groupe" />
			</li>

			</ul>
			</form>
				';
		return $res;
	}
	
	public function render($selecteur) {
		$res;
		
		switch ($selecteur) {
			case self::INSCRIPTION :
				$res = $this->inscriptionLogement($this->logem['places']);
				break;
	}
	
	$app=\Slim\Slim::getInstance();
	$routecss=$app->urlFor('css');
	$routejs=$app->urlFor('js');
	$routefonts=$app->urlFor('fonts');
	$routeimg=$app->urlFor('img');
	$h=new VueHeader();
	$header=$h->headerToHtml();
	$html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>lebonlogement</title>
		<link rel="icon" type="image/png" href="$routeimg/icon_house.png" />
		<link rel="stylesheet" href="$routecss/style.css">
		<!--Import Google Icon Font-->
		 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		 <!--Import materialize.css-->
		 <link type="text/css" rel="stylesheet" href="$routecss/materialize.min.css"  media="screen,projection"/>
		   <link type="text/css" rel="stylesheet" href="style.css"  media="screen,projection"/>
		 <!--Let browser know website is optimized for mobile-->
		 <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		 <script type="text/javascript" src="$routejs/materialize.min.js"></script>
	</head>
	<body>
	$header
	<div class="content">
	$res
	</div>
	</body>
</html>
EOT;
	echo $html;
	}
}
	