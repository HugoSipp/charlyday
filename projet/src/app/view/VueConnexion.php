<?php

namespace app\view;

class VueConnexion {
	const CONNEXION = 0;

	function __construct() {
	}

	public function afficher_connexion(){
		$app=\Slim\Slim::getInstance();
		$res = '<br>
				<h2 class="titre">Connexion</h2>
			<form class="form-style-9" action="'.$app->urlFor('sendConnexion').'" method="post">
			<ul>
			<li>
			<input type="text" name="login"  class="field-style field-split align-left" placeholder="Login" />
			<input type="password" name="mdp"  class="field-style field-split align-right" placeholder="Mot de passe" />
			</li>
			<li>
			<input type="submit" value="Connexion" />
			</li>

			</ul>
			</form>
				';
		
		return $res;
	}

	public function render($selecteur) {
		$res;

		switch ($selecteur) {
			case self::CONNEXION :
				$res = $this->afficher_connexion();
				break;
		}
		$app=\Slim\Slim::getInstance();
		$routecss=$app->urlFor('css');
		$routejs=$app->urlFor('js');
		$routefonts=$app->urlFor('fonts');
		$routeimg=$app->urlFor('img');
		$h=new VueHeader();
		$header=$h->headerToHtml();
		$html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>lebonlogement</title>
		<link rel="icon" type="image/png" href="$routeimg/icon_house.png" />
		<link rel="stylesheet" href="$routecss/style.css">
		<!--Import Google Icon Font-->
		 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		 <!--Import materialize.css-->
		 <link type="text/css" rel="stylesheet" href="$routecss/materialize.min.css"  media="screen,projection"/>
		   <link type="text/css" rel="stylesheet" href="style.css"  media="screen,projection"/> 
		 <!--Let browser know website is optimized for mobile-->
		 <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		 <script type="text/javascript" src="$routejs/materialize.min.js"></script>
	</head>
	<body>
	$header
	<div class="content">
	$res
	</div>
	</body>
</html>
EOT;

		echo $html;
	}
}
