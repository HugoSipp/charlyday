<?php

namespace app\view;

class VueHeader{

	private $location;

	function __construct(){

	}

	function headerToHtml(){
		$app=\Slim\Slim::getInstance();

		$res='
<script type="text/javascript">
  $(document).ready(function(){$(".button-collapse").sideNav();})
</script>

	<nav>
    <div class="nav-wrapper #f4511e deep-orange darken-1">
      <a href="'.$app->urlFor('racine').'" style="font-family: impact;margin-left:2%; " class="brand-logo">lebonlogement</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
		<li><a href="'.$app->urlFor('racine').'">Logements</a></li>
		<li><a href="'.$app->urlFor('users').'">Utilisateurs</a></li>
		<li><blockquote></blockquote></li>';
     if(!isset($_SESSION['isConnected'])){
		$res.='<li><a href="'.$app->urlFor('connexion').'">Connexion</a></li>';
	 }
	 else{
		$res.='<li><a href="'.$app->urlFor('logout').'">Déconnexion</a></li>';
	 }
     $res.='</ul>
     <ul class="side-nav" id="mobile-demo">';
	 if(!isset($_SESSION['isConnected'])){
		$res.='<li><a href="'.$app->urlFor('connexion').'">Connexion</a></li>';
	 }
	 else{
		$res.='<li><a href="'.$app->urlFor('logout').'">Déconnexion</a></li>';
	 }
     $res.='<li><a href="'.$app->urlFor('racine').'"> Logements</a></li>
        <li><a href="'.$app->urlFor('racine').'">Logements</a></li>
        <li><a href="'.$app->urlFor('users').'">Utilisateurs</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="'.$app->urlFor('racine').'">Logements</a></li>
        <li><a href="'.$app->urlFor('users').'">Utilisateurs</a></li>
      </ul>
    </div>
  </nav>';

		return $res;
	}
	
	function headerFirst(){
		
			$app=\Slim\Slim::getInstance();
		
			$res='
<script type="text/javascript">
  $(document).ready(function(){$(".button-collapse").sideNav();})
</script>
		
	 <nav>
    <div class="nav-wrapper #f411e deep-orange darken-1">
      <a href="#!" style="font-family: impact;margin-left:2%; " class="brand-logo">lebonlogement</a>
      <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#" style="margin-right:2%;">Connexion</a></li>
        <li><a href="#" style="margin-right:2%;">Inscription</a></li>
      </ul>
      <ul class="side-nav" id="mobile-demo">
        <li><a href="'.$app->urlFor('connexion').'">Connexion</a></li>
        <li><a href="'.$app->urlFor('inscription').'">Inscription</a></li>
      </ul>
    </div>
  </nav>
  ';
		
			return $res;
		
	}

}
