<?php

namespace app\view;

class VueUser {
	const LISTE_USER = 0;
	const ONE_USER = 1;

	private $liste_user;
	function __construct($l) {
		$this->liste_user = $l;
	}

	public function afficher_liste($liste){
		$app=\Slim\Slim::getInstance();
		$res = '<div class="row">
							<div class="col s12">
								<h4>Utilisateurs :</h4>
							</div>
						</div>';
		//var_dump($liste);
		foreach($liste as $key=>$val){
			$res.='
			<div class="row">
				<div class="col s12 m4 l2">
				</div>
				<div class="col s12 m4 l8">
	        <div class="card-panel grey lighten-5 z-depth-1">
							<div style="cursor: pointer;" class="row valign-wrapper"  onclick="document.location=\''.$app->urlFor('user',array('id'=>$val['id'])).'\';">
		            <div class="col s2">
		              <img src="' . $app->urlFor('img') . '/user/' . $val["id"] . '.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
		            </div>
		            <div class="col s10">
		              <span class="black-text">
											<h5>'.$val['nom'].'</h5>
											<p>'.$val['message'].'</p>
		              </span>
		            </div>
		          </div>
					</div>
				</div>
				<div class="col s12 m4 l2">
				</div>
			</div>';
		}
		return $res;
	}

	public function afficher_user($val){
		$app=\Slim\Slim::getInstance();
		return '<div class="col s12 m4 l8">
			<div class="card-panel grey lighten-5 z-depth-1">
					<div class="row valign-wrapper">
						<div class="col s2">
							<img src="' . $app->urlFor('img') . '/user/' . $val["id"] . '.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
						</div>
						<div class="col s10">
							<span class="black-text">
									<h5>'.$val['nom'].'</h5>
									<p>'.$val['message'].'</p>
							</span>
						</div>
					</div>
			</div>
		</div>';
	}


	public function render($selecteur) {
		$res;

		switch ($selecteur) {
			case self::LISTE_USER :
				$res = $this->afficher_liste($this->liste_user);
				break;
			case self::ONE_USER :
				$res = $this->afficher_user($this->liste_user);
				break;
		}
		$app=\Slim\Slim::getInstance();
		$routecss=$app->urlFor('css');
		$routejs=$app->urlFor('js');
		$routefonts=$app->urlFor('fonts');
		$routeimg=$app->urlFor('img');
		$h=new VueHeader();
		$header=$h->headerToHtml();
		$html= <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>lebonlogement</title>
		<link rel="icon" type="image/png" href="$routeimg/icon_house.png" />
		<link rel="stylesheet" href="$routecss/style.css">
		<!--Import Google Icon Font-->
		 <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		 <!--Import materialize.css-->
		 <link type="text/css" rel="stylesheet" href="$routecss/materialize.min.css"  media="screen,projection"/>

		 <!--Let browser know website is optimized for mobile-->
		 <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		 <script type="text/javascript" src="$routejs/materialize.min.js"></script>
	</head>
	<body>
	$header
	<div class="content">
	$res
	</div>
	</body>
</html>
EOT;

		echo $html;
	}
}
