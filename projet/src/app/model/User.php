<?php

namespace app\model;
	
class User extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'user';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function groupe(){
		return $this->BelongsTo('\app\model\Groupe','id');
	}
}