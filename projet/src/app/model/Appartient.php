<?php

namespace app\model;
	
class Appartient extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'appartient';
	public $timestamps = false;
	
	public function groupe(){
		return $this-belongsTo('\giftbox\model\Groupe','id');
	}
	
	public function user(){
		return $this->belongsTo('\giftbox\model\User','id');
	}
}