<?php

namespace app\model;
	
class Groupe extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'groupe';
	protected $primaryKey = 'id';
	public $timestamps = false;
	

	public function user(){
		return $this->hasMany('\app\model\Appartient','iduser');
	}
}