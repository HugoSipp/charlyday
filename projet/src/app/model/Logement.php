<?php

namespace app\model;

class Logement extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'logement';
    protected $primaryKey ='id';
    public $timestamps = false ;
}
?>