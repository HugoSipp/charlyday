<?php

namespace app\controler;

use \app\view\VueConnexion;
use \app\model\User;

class ControlerConnexion {
	
	public function pageConnexion() {
		$vueConn = new VueConnexion();
		$vueConn->render(VueConnexion::CONNEXION);
	}
	
	public function sendConnexion(){
		$app=\Slim\Slim::getInstance();
		if(isset($_POST['login']) && isset($_POST['mdp']) && !isset($_SESSION['isConnected'])){
			$user = user::where('id','=',1)->first();
			if(!is_null($user) && password_verify($_POST['mdp'], $user->mdp)){
				$_SESSION['login']=$_POST['login'];
				$_SESSION['mdp']=$_POST['mdp'];
				$_SESSION['isConnected']=true;
				header ('Location: '.$app->urlFor('racine') );
				exit;
			}
			header ('Location: '.$app->urlFor('connexion') );
			exit;
		}
		header ('Location: '.$app->urlFor('connexion') );
		exit;
	}
	
	public static function verify(){
		if(isset($_SESSION['login']) && isset($_SESSION['mdp']) && $_SESSION['isConnected']){
			$user = User::where('login','=',$_SESSION['login'])->first();
			if(!is_null($user) && $user->mdp == $_SESSION['mdp']){
				return true;
			}
			unset($_SESSION['login']);
			unset($_SESSION['mdp']);
			unset($_SESSION['isConnected']);
		}
		return false;
	}
	
	public function logout(){
		$app=\Slim\Slim::getInstance();
		unset($_SESSION['login']);
		unset($_SESSION['mdp']);
		unset($_SESSION['isConnected']);
		header ('Location: '.$app->urlFor('racine') );
		exit;
	}
}
