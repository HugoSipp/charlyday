<?php

namespace app\controler;

use \app\model\User;
use \app\view\VueUser;


class ControlerUser {

	public function afficherListeUtilisateurs() {
		$liste_user = User::get();
		$vueUser = new VueUser($liste_user->toArray());
		$vueUser->render(VueUser::LISTE_USER);
	}

	public function afficherUtilisateur($id) {
		$user = User::where('id','=',$id)->first();
		if(isset($user)){
			$vueUser = new VueUser($user->toArray());
			$vueUser->render(VueUser::ONE_USER);
		}
	}



}
