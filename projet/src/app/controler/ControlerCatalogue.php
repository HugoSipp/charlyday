<?php

namespace app\controler;

use \app\view\VueCatalogue;
use \app\model\Logement;

class ControlerCatalogue {
	public function index() {
		if (isset($_SESSION['done'])){
			$listLogement = Logement::get ();
			$vueLogem = new VueCatalogue ( $listLogement->toArray () );
			$vueLogem->render ( VueCatalogue::LIST_LOGEMENT );
		} else {
			$vue = new VueCatalogue ();
			$vue->render(VueCatalogue::INDEX);
		}
	}

	public function continuer(){
		$_SESSION['done']=true;
		$this->index();
	}

	public function afficherLogement($id) {
		$logement = Logement::where('id','=',$id)->first();
		if(isset($logement)){
			$vueCatalogue = new VueCatalogue($logement->toArray());
			$vueCatalogue->render(VueCatalogue::ONE_LOGEMENT);
		}
	}
}
