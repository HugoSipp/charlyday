<?php

namespace app\controler;

use \app\view\VueInscription;

class ControlerInscription {
	public function pageInscription() {
		$vueInsc = new VueInscription();
		$vueInsc->render(VueInscription::INSCRIPTION);
	}
}
