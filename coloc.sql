
/*!40101 SET NAMES utf8 */;

CREATE TABLE IF NOT EXISTS `logement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `places` int(11) NOT NULL,
  `libelle` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;


INSERT INTO `logement` (`id`, `places`,`libelle`) VALUES
(1, 3,'Petit appartement sympatoche'),
(2, 3,'Appartement centre-ville'),
(3, 4,'Studio'),
(4, 4,'Villa'),
(5, 4,'Petite Maison dans la prairie'),
(6, 4,'Grande maison'),
(7, 4,'Quartier calme'),
(8, 5,'Maison près du cimetière'),
(9, 5,'Quartier pas calme'),
(10, 6,'Appart à teuf'),
(11, 6,'Maison de vieux'),
(12, 6,'Moulin du lac'),
(13, 7,'grand F2'),
(14, 7,'Ancienne Maison du Général Leclerc'),
(15, 8,'Building superposé'),
(16, 2,'Maison Lego'),
(17, 2,'Maison en bois d\'olivier'),
(18, 2,'Superbe palace'),
(19, 2,'Trop chère pour toi'),
(20, 2,'IUT'),
(21, 3,'Chambre du crous'),
(22, 3,'Villa avec vue sur les bidons-ville'),
(23, 3,'Maison de benoit'),
(24, 3,'Maison de Léo'),
(25, 3,'Palace d\'Alexandre');


CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `login` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mdp` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;


INSERT INTO `user` (`id`, `nom`, `message`) VALUES
(1, 'Jeanne', 'aime la musique ♫'),
(2, 'Paul', 'aime cuisiner ♨ ♪'),
(3, 'Myriam', 'mange Halal ☪'),
(4, 'Nicolas', 'ouvert à tous ⛄'),
(5, 'Sophie', 'aime sortir ♛'),
(6, 'Karim', 'aime le soleil ☀'),
(7, 'Julie', 'apprécie le calme ☕'),
(8, 'Etienne', 'accepte jeunes et vieux ☯'),
(9, 'Max', 'féru de musique moderne ☮'),
(10, 'Sabrina', 'aime les repas en commun ⛵☻'),
(11, 'Nathalie', 'bricoleuse ⛽'),
(12, 'Martin', 'sportif ☘ ⚽ ⚾ ⛳'),
(13, 'Manon', ''),
(14, 'Thomas', ''),
(15, 'Léa', ''),
(16, 'Alexandre', ''),
(17, 'Camille', ''),
(18, 'Quentin', ''),
(19, 'Marie', ''),
(20, 'Antoine', ''),
(21, 'Laura', ''),
(22, 'Julien', ''),
(23, 'Pauline', ''),
(24, 'Lucas', ''),
(25, 'Sarah', ''),
(26, 'Romain', ''),
(27, 'Mathilde', ''),
(28, 'Florian', '');

CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11),
  `idlogement` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;

CREATE TABLE IF NOT EXISTS `appartient` (
  `iduser` int(11),
  `idgroupe` int(11),
  PRIMARY KEY (`iduser`, `idgroupe`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;